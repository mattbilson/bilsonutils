/*global define */
define(['./utils/FileSystemUtils', './utils/OtherUtils', './utils/Polyfills'], function (FileSystemUtils, OtherUtils, Polyfills) {
	'use strict';

	var utils = {

		polyfills : Polyfills,	//these normally don't need calling, but are automatically applied to the window

		otherUtils : OtherUtils,
		fileSystemUtils : FileSystemUtils
		
	}

	return utils;

});