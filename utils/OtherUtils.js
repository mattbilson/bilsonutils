/*global define */
define([], function () {
	'use strict';

	var utils = {
		
		//get a query string parameter
		//https://developer.mozilla.org/en-US/docs/Web/API/window.location?redirectlocale=en-US&redirectslug=DOM%2Fwindow.location    
		getQueryVar : function(key) {
		  return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
		},
		
	}

	return utils;

});